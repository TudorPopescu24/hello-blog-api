using Microsoft.EntityFrameworkCore;
using System;

namespace hello_blog_api.Repository
{
    public class BlogDbContext : DbContext
    {
        public string DbPath { get; }

        public BlogDbContext(DbContextOptions<BlogDbContext> options)
            : base(options)
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "blogging.db");
        }
 
        public DbSet<BlogPost> BlogPosts { get; set; }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");
    }
}