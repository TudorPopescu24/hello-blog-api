using Microsoft.AspNetCore.Mvc;
using hello_blog_api.Models;
using hello_blog_api.TestData;
using System.Net;
using System.Collections.Generic;

namespace hello_blog_api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BlogPostController: Controller
    {
        ///<summary>
        ///Creates a blog using the payload information.
        ///Endpoint url: api/v1/BlogPost
        ///</summary>
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created, Type = typeof(BlogPostModel))]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult CreateBlogPost([FromBody] BlogPostModel blogPost)
        {
            try
            {
                if (!BlogPostTestData.ValidateBlogPostModel(blogPost))
                {
                    return BadRequest("Model is not valid!");
                }
               
                if(!BlogPostTestData.AddBlogPost(blogPost))
                {   
                     return StatusCode((int)HttpStatusCode.InternalServerError);
                }
                
                return CreatedAtAction(nameof(CreateBlogPost), blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        public IActionResult GetAllBlogPosts()
        {
            try
            {
                List<BlogPostModel> blogPosts = BlogPostTestData.GetAllBlogPosts();
                return Ok(blogPosts);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("blogPostId/{blogPostId}")]
        public IActionResult GetBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostById(blogPostId);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        //Exercitiul 1 - Get Blog Post by Tag(Label)
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("blogPostTag/{blogPostTag}")]
        public IActionResult GetBlogPostByTag([FromRoute] string blogPostTag)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.GetBlogPostByTag(blogPostTag);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        //Exercitiul 2 - Delete Blog Post By Id
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("deleteBlogPost/{blogPostId}")]
        public IActionResult DeleteBlogPostById([FromRoute] int blogPostId)
        {
            try
            {
                bool result = BlogPostTestData.DeleteBlogPostById(blogPostId);
                if (result == false)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok();
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        //Exercitiul 3 - Update Blog Post By Id
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("updateBlogPost/{blogPostId}/{blogPostTitle}/{blogPostLabel}/{blogPostContent}")]
        public IActionResult UpdateBlogPostById([FromRoute] int blogPostId, 
        [FromRoute] string blogPostTitle, [FromRoute] string blogPostLabel, [FromRoute] string blogPostContent)
        {
            try
            {
                BlogPostModel blogPost = BlogPostTestData.UpdateBlogPostById(blogPostId, blogPostTitle, blogPostLabel, blogPostContent);
                if (blogPost == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound);
                }
                return Ok(blogPost);
            }
            catch
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}